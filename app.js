const createError = require('http-errors');
const express = require('express');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const app = express();
const mongoose = require('mongoose');
const winston = require('winston');
const config  = require('./config/config.json');
const auth = require('./auth/basic');
const busRouter = require('./app/bus/route');

const myconsole = new winston.transports.Console();

winston.add(myconsole);
if (config.env == 'dev') {
  winston.level = 'debug'
} else {
  winston.level = 'info'
}

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

app.use(auth.basicAuth)
app.use('/bus', busRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

// connecting to mongoose
mongoose.connect(config.mongodb_connection_string, {useNewUrlParser: true}).catch (err => {
  winston.error("error connecting to mongoDB",{'err': err});
});

mongoose.connection.on('error', err => {
  winston.error("error in mongoose connection", {'err': err});
});

module.exports = app;

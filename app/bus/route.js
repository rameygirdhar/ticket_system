/**
 * @file - routes for bus api
 */

const express = require('express');
const router = express.Router();
const ctrl = require('./controllers')

router.post('/', ctrl.createBus);
router.get('/:bus_id/tickets', ctrl.getTickets);
router.get('/:bus_id/tickets/:ticket_num', ctrl.getTickets);
router.put('/:bus_id/tickets/:ticket_num', ctrl.updateTicket);
router.get('/:bus_id/tickets/:ticket_num/person', ctrl.getPerson);
router.delete('/:bus_id', ctrl.resetBus);
module.exports = router;

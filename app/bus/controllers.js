/**
 * @file - controllers for bus api
 */

const ticket = require('./../ticket/ticket');
const bus = require('./bus');
const config = require('./../../config/config.json');

/**
 * @function - handler for creating a new bus
 * @param {obj} req 
 * @param {obj} res 
 */
const createBus = async (req, res) => {
    if (req.body.name == undefined || req.body.name.trim() === '') {
        return res.status(400).json({"status": "failure", "error": "invalid bus name"});
    }
    const id = await bus.newBus(req.body);
    if (id == undefined) {
        return res.status(500).json({"status": "failure", "error": "internal server error"});
    }
    return res.status(201).json({"status": "success", "id": id});
};

/**
 * @function - handler for getting bus tickets based on user request
 * @param {obj} req 
 * @param {obj} res 
 */
const getTickets = async (req, res) => {
    const ticketStatus = req.query['status'];
    const busID = req.params.bus_id;
    const ticketNum = req.params.ticket_num;
    if (busID.trim() == '') {
        return res.status(400).json({"status": "failure", "error": "invalid busID"});
    }

    if (ticketStatus != undefined && ticketStatus !== 'open' && ticketStatus !== 'closed') {
        return res.status(400).json({status: "failure", error: "invalid ticket status"});
    }

    if (ticketNum < 1 || ticketNum > config.num_seats) {
        return res.status(400).json({"status": "filure", "error": `invalid ticket number, ticket number > 1 and ticket number < ${config.num_seats}`});
    }
    const exists = await bus.checkBus(busID);
    if (exists == undefined) {
        return res.status(500).json({"status": "failure", "error": "internal server error"});
    }

    if (!exists) {
        return res.status(404).json({"status": "failure", "error": "bus not found"});
    }

    const tickets = await ticket.getTickets(busID, ticketStatus, ticketNum);
    return res.status(200).json({"status": "success", "tickets": tickets});
};

/**
 * @function - handler to get person details for a tickets
 * @param {obj} req 
 * @param {obj} res 
 */
const getPerson = async (req, res) => {
    const busID = req.params.bus_id;
    const ticketNum = req.params.ticket_num;

    if (busID.trim() == '') {
        return res.status(400).json({"status": "failure", "error": "invalid busID"});
    }

    if (ticketNum.trim() == '') {
        return res.status(400).json({"status": "failure", "error": "invalid ticket number"});
    }

    const ticketNumber = parseInt(ticketNum);
    
    if (ticketNumber == NaN) {
        return res.status(400).json({"status": "failure", "error": "invalid ticket number, expects integer"});
    }

    if (ticketNum < 1 || ticketNum > config.num_seats) {
        return res.status(400).json({"status": "filure", "error": `invalid ticket number, ticket number > 1 and ticket number < ${config.num_seats}`});
    }

    const exists = await bus.checkBus(busID);
    if (exists == undefined) {
        return res.status(500).json({"status": "failure", "error": "internal server error"});
    }

    if (!exists) {
        return res.status(404).json({"status": "failure", "error": "bus not found"});
    }

    let personDetails = await ticket.getPersonDetails(busID, ticketNum);
    if (personDetails == null) {
        return res.status(404).json({"status": "failue", "error": "person not found"});
    }

    if (exists == undefined) {
        return res.status(500).json({"status": "failure", "error": "internal server error"});
    }

    return res.status(200).json({"status": "success", "person": {firstName: personDetails.name.firstName, lastName: personDetails.name.lastName, age: personDetails.age}});
};

/**
 * @function - handler to update ticket details
 * @param {obj} req 
 * @param {obj} res 
 */
const updateTicket = async (req, res) => {
    const busID = req.params.bus_id;
    const ticketNum = req.params.ticket_num;

    if (busID.trim() == '') {
        return res.status(400).json({"status": "failure", "error": "invalid busID"});
    }

    if (ticketNum.trim() == '') {
        return res.status(400).json({"status": "failure", "error": "invalid ticket number"});
    }

    const ticketNumber = parseInt(ticketNum);
    
    if (ticketNumber == NaN) {
        return res.status(400).json({"status": "failure", "error": "invalid ticket number, expects integer"});
    }

    if (ticketNum < 1 || ticketNum > config.num_seats) {
        return res.status(400).json({"status": "filure", "error": `invalid ticket number, ticket number > 1 and ticket number < ${config.num_seats}`})
    }
    
    const exists = await bus.checkBus(busID)
    if (exists == undefined) {
        return res.status(500).json({"status": "failure", "error": "internal server error"});
    }

    if (!exists) {
        return res.status(404).json({"status": "failure", "error": "bus not found"});
    }

    if (req.body.status != 'open' && req.body.status != 'closed') {
        return res.status(400).json({"status": "failed", "error": "invalid ticket status"});
    }

    if (req.body.status == 'closed' && req.body.person == undefined) {
        return res.status(400).json({"status": "failed", "error": "person details required to close a ticket"});
    }

    const updatedTicket = await ticket.updateTicket({ticketNum: ticketNumber, busID: busID, person: req.body.person, status: req.body.status});
    if (updatedTicket == undefined) {
        return res.status(500).json({"status": "failure", "error": "internal server error"});
    }

    if (typeof updatedTicket === 'string') {
        return res.status(400).json({"status": "failure", "error": updatedTicket});
    }

    return res.status(200).json({"status": "success", "busID": busID, "seatNumber": ticketNum});
};

/**
 * @function - reset all the bus seats
 * @param {obj} req 
 * @param {obj} res 
 */
const resetBus = async (req, res) => {
    const busID = req.params.bus_id;
    if (busID.trim() == '') {
        return res.status(400).json({"status": "failure", "error": "invalid busID"});
    }

    const exists = await bus.checkBus(busID)
    if (exists == undefined) {
        return res.status(500).json({"status": "failure", "error": "internal server error"});
    }

    if (!exists) {
        return res.status(404).json({"status": "failure", "error": "bus not found"});
    }

    const nModified = await ticket.resetTickets(busID);
    if (nModified == undefined) {
        return res.status(500).json({"status": "failure", "error": "internal server error"});
    }
    return res.status(200).json({"status": "success", "tickets_modified": nModified})
}

module.exports = {
    createBus,
    getTickets,
    updateTicket,
    getPerson,
    resetBus,
};
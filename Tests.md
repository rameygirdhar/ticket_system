## Test Cases

Folowwing is the list of test cases - 

* DO POST `/bus` request with name `bus1`. __expected:__ API should return 201 with bus ID
* Do the same request without name,__expected:__ API should return 400 bad request.
* Do the same request with empty name string,__expected:__ API should return 400 bad request
* Do GET `/bus/:bus_id/tickets` request. __expected:__ this should list all tickets with status open
* Do GET request with wrong bus number. __expected:__ API should return 404 not found.
* Do PUT `/bus/:bus_id/tickets/4` request to update seat `4` of bus with person details and status `closed`. __expected:__ API should return 200
* Do GET `/bus/:bus_id/tickets/4` request. __expected:__ this should list ticket 4 status closed
* Do GET `/bus/:bus_id/tickets/41` request. __expected:__ API should return 400 bad request
* Do GET `/bus/:bus_id/tickets?status=open` request. __expected:__ API should return all tickets expcept 4
* Do GET `/bus/:bus_id/tickets?status=closed` request. __expected:__ API should ticket 4 status
* Do PUT `/bus/:bus_id/tickets/10` request without status param. __expected:__ API should return 400 bad request with invalid ticket status
* Do PUT `/bus/:bus_id/tickets/10` request without person param and status `closed`. __expected:__ API should return 400 bad request with person required to close a ticket.
* Do PUT `/bus/:bus_id/tickets/10` request with wrong param for person like firstName/age. __expected:__ API should return 400 bad request with details of invalid input.
* Do PUT `/bus/:bus_id/tickets/10` request with wrong bus. __expected:__ API should return 404 bus not found.
* Do PUT `/bus/:bus_id/tickets/-1` request. __expected:__ API should return 400 invalid ticket number.
* Do PUT `/bus/:bus_id/tickets/45` request. __expected:__ API should return 400 invalid ticket number.
* Do GET `/bus/:bus_id/tickets/4/person` request. __expected:__ API should return 200 with person details.
* Do GET `/bus/:bus_id/tickets/5/person` request. __expected:__ API should return 404 with person not found.
* Do PUT `/bus/:bus_id/tickets/4` request and set it to open. __expected:__ API should return 200.
* Do GET `/bus/:bus_id/tickets/4/person` request. __expected:__ API should return 404.
* Do a DELETE `/bus/:bus_id` request. __expected:__ API should return 200 with number of documents modified.
* Do a DELETE `/bus/:bus_id` without admin credentials. __expected:__ API should return 401.
* Do any api request with wrong/invalid credentials. __expected:__ API should return 401.


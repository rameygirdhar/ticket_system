/**
 * @file - contains data layer logic for tickets
 */

const mongoose = require('mongoose');
const winston = require('winston');

/**
 * ticket schema and model
 */
const ticketSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    number: {
        type: Number,
        required: true
    },
    bus: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Bus'
    },
    person: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Person'
    },
    status: {
        type: String,
        required: true,
    }
});

const Ticket = mongoose.model('Ticket', ticketSchema);

/**
 * @function - save ticket status
 * @param {number} seatNum 
 * @param {string} busID 
 */
const saveTicket = async (seatNum, busID) => {
    winston.info('creating new ticket', {
        'bus': busID,
        'number': seatNum
    });

    const ticket = new Ticket({
        _id: new mongoose.Types.ObjectId(),
        bus: busID,
        status: 'open',
        number: seatNum
    });

    try {
        let createdTicket = await ticket.save();
        winston.info('creation of ticket successful', {'id': createdTicket._id, 'num': seatNum, 'bus': busID});
        return true
    } catch(err) {
        winston.error('creation of ticket failed', {'bus': busID, 'num': seatNum});
        return false
    }
};

/**
 * @function get ticket details using bus ID, ticket status and number
 * @param {string} busID 
 * @param {string} ticketStatus 
 * @param {number} ticketNum 
 */
const getTickets = async (busID, ticketStatus, ticketNum) => {
    winston.info('getting bus tickets', {
        'bus': busID,
        'ticketStatus': ticketStatus
    });
    queryObj = {bus: mongoose.Types.ObjectId(busID)};

    if (ticketStatus != undefined) {
        queryObj.status = ticketStatus;
    }

    if (ticketNum != undefined) {
        queryObj.number = ticketNum;
    }

    try {
        tickets = await Ticket.find(queryObj);
        return tickets;
    } catch(err) {
        winston.error('error getting tickets',{'err': err, 'bus': busID});
    }
};

/**
 * @function updateTicket - updates ticket's status
 * @param {string} busID 
 * @param {number} ticketNum 
 * @param {string} ticketStatus 
 * @param {string} personID 
 */
const updateTicket = async (busID, ticketNum, ticketStatus, personID) => {
    winston.info('updating bus ticket', {
        'busID': busID,
        'ticketStatus': ticketStatus,
        'ticketNum': ticketNum,
        'personID': personID
    });

    const filter = {bus: busID, number: ticketNum};

    const update = {};
    if (ticketStatus != undefined) {
        update.status = ticketStatus;
    }

    if (personID != undefined) {
        update.person = personID;
    }

    if (personID == null) {
        update.person = undefined;
    }


    try {
        let doc = await Ticket.findOneAndUpdate(filter, update);
        winston.info('update ticket successful', {
            'busID': busID,
            'ticketStatus': ticketStatus,
            'ticketNum': ticketNum,
            'personID': personID
        });
        return doc
    } catch(err) {
        winston.error('update ticket failed', {
            'busID': busID,
            'ticketStatus': ticketStatus,
            'ticketNum': ticketNum,
            'personID': personID,
            'err': err
        })
    }
};

/**
 * @function - get's person's ID based on bus ID and ticket number
 * @param {string} busID 
 * @param {number} ticketNum 
 */
const getPersonID = async (busID, ticketNum) => {
    winston.info('getting person ID', {'busID': busID, 'ticketNum': ticketNum});

    try {
        let ticket = await Ticket.findOne({bus: busID, number: ticketNum});
        if (ticket.person == undefined) {
            return null;
        }
        return ticket.person._id;
    } catch(err) {
        winston.error('get person failed', {'busID': busID, 'seatNum': ticketNum});
    }
};

/**
 * @function resets all the tickets
 * @param {string} busID 
 */
const resetTickets = async(busID) => {
    winston.info('reseting tickets', {'busID': busID});
    try {
        const res = await Ticket.updateMany({bus: busID}, {person: null, status: 'open'});
        winston.info(`updated ${res.nModified} seats`, {'bus': busID});
        return res
    }  catch(err) {
        winston.error('reset bus tickets failed', {'bus': busID, 'err': err});
    }
};

module.exports = {
    saveTicket,
    getTickets,
    updateTicket,
    getPersonID,
    resetTickets
};
/**
 * @file - auth specific middleware
 */

const config = require('./../config/config.json');

/**
 * @function - basic auth logic
 * @param {obj} req 
 * @param {obj} res 
 * @param {obj} next 
 */
const basicAuth = (req, res, next) => {
    const username = config.auth.username;
    const password = config.auth.password;
    const adminUsername = config.auth.admin.username; 
    const adminPassword = config.auth.admin.password; 
    
    

    if (!req.headers.authorization || req.headers.authorization.indexOf('Basic ') === -1) {
        return res.status(401).json({error: 'missing authorization header'});
    }

    const base64Credentials =  req.headers.authorization.split(' ')[1];
    const credentials = Buffer.from(base64Credentials, 'base64').toString('ascii');
    const [authUser, authPass] = credentials.split(':');

    if (req.method === 'DELETE') {
        if (authUser != adminUsername || authPass != adminPassword) {
            return res.status(401).json({error: 'invalid auth credentials'});
        }
    } else {
        if ((authUser != adminUsername || authPass != adminPassword) && (authUser != username || authPass != password)) {
            return res.status(401).json({error: 'invalid auth credentials'});
        }
    }
    
    next();
}

module.exports = {
    basicAuth
};
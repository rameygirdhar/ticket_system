/**
 * @file - contains Data layer logic for bus
 */

const mongoose = require('mongoose');
const winston = require('winston');

/**
 * bus model and schema definition
 */
const busSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    name: String,
    created: {
        type: Date,
        default: Date.now,
    }
});

const Bus = mongoose.model('Bus', busSchema);

/**
 * @function - stores new bus in mongoDB
 * @param {string} busName required - bus name
 */
const saveBus = async (busName) => {
    winston.info('creating new bus', {'name': busName});
    const bus = new Bus({
        _id: new mongoose.Types.ObjectId(),
        name: busName,
    });

    try {
        let createdBus = await bus.save();
        winston.info('creation of new bus successful', {'id': createdBus._id, 'name': busName});
        return createdBus._id;
    } catch(err) {
        winston.error('error creating new bus', {'name': busName, 'err': err});
    }
};

/**
 * @function - check if bus exists in MongoDB
 * @param {string} busID required
 */
const busExists = async (busID) => {
    winston.info('checking bus exists', {'id': busID});

    if (!mongoose.Types.ObjectId.isValid(busID)) {
        return false;
    }

    try {
        const exists = await Bus.exists({_id: mongoose.Types.ObjectId(busID)});
        return exists;
    } catch(err) {
        winston.error('error checking whether bus exists', {'id': busID, 'err': err});
    }
}

module.exports = {
    saveBus,
    busExists,
};
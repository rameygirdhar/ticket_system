/**
 * @file - contains data layer logic for a person
 */

const mongoose = require('mongoose');
const winston = require('winston');

/**
 * model and schema for person
 */
const personSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    name: {
        firstName: {
            type: String,
            required: true
        },
        lastName: String
    },
    age: {
        type: Number,
        required: true
    },
    created: {
        type: Date,
        default: Date.now
    }
});

const Person = mongoose.model('Person', personSchema);

/**
 * @function - saves a person in DB
 * @param {string} firstName 
 * @param {string} lastName 
 * @param {number} age 
 */
const savePerson = async (firstName, lastName, age) => {
    winston.info('creating new person', {'firstName': firstName, 'lastName': lastName, 'age': age});
    const person = new Person({
        _id: new mongoose.Types.ObjectId(),
        name: {
            firstName: firstName,
            lastName: lastName,
        },
        age: age
    });

    try {
        let createdPerson = await person.save();
        winston.info('successfully created person', {'id': createdPerson._id, 'firstName': firstName, 'lastName': lastName, 'age': age});
        return createdPerson._id;
    } catch(err) {
        winston.error('error creating new person', {'id': createdPerson._id, 'firstName': firstName, 'lastName': lastName, 'age': age});
    }
};

/**
 * @function - fetch a person's details with ID
 * @param {string} personID 
 */
const getPerson = async (personID) => {
    winston.info('get person details for a ticket', {'id': personID});

    console.log(personID);
    try {
        let personDetails = Person.findById(mongoose.Types.ObjectId(personID));
        winston.info('get person details successful');
        return personDetails
    } catch(err) {
        winston.error('get person failed', {'id': personID, 'err': err});
    }
};

module.exports = {
    savePerson,
    getPerson,
};

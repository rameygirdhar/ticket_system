# Ticket System

This repo contains code for REST APIs for bus ticketing system

### Directory Structure
```
.
├── Readme.md
├── app
│   ├── bus
│   │   ├── bus.js
│   │   ├── controllers.js
│   │   ├── route.js
│   │   └── schema.js
│   ├── person
│   │   ├── person.js
│   │   └── schema.js
│   └── ticket
│       ├── schema.js
│       └── ticket.js
├── app.js
├── auth
│   └── basic.js
├── bin
│   └── www
├── config
│   └── config.json
├── package-lock.json
└── package.json

```

Tests.md lists the test cases
/**
 * @file - person specific application logic 
 */
const schema = require('./schema');

/**
 * @function - save person details
 * @param {firstName} string - person's first name
 * @param {lastName} string - person's last name
 * @param {age} number - person's age
 */
const savePerson = async ({firstName, lastName, age}) => {
    if (firstName == undefined || firstName.trim() == '') {
        return {error: 'invalid firstName'};
    }

    if (lastName.trim() == '') {
        return {error: 'invalid lastName'};
    }

    if (age == undefined || age < 0 ){
        return {error: 'invalid age'};
    }

    let personID = await schema.savePerson(firstName, lastName, age);
    if (personID == undefined) {
        return;
    }
    return personID;
};

/**
 * @function - gets person details
 * @param {string} personID - ID of person to get details
 */
const getPerson = async (personID) => {
    if (personID == undefined) {
        return;
    }

    return await schema.getPerson(personID);
}

module.exports = {
    savePerson,
    getPerson
}
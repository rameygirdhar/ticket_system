/**
 * @file - ticket specific application logic
 */

const config = require('../../config/config.json');
const schema = require('./schema');
const personS = require('./../person/person');

/**
 * @function - createNewTickets - create new bus tickets from 1 to max_num
 * @param {string} busID 
 */
const createNewTickets = async (busID) => {
    if (busID == undefined) {
        return;
    }
    for (i = 1; i <= config.num_seats; i++) {
        success = await schema.saveTicket(i, busID);
        if (!success) {
            return false;
        }
    }
    return true;
}

/**
 * @function - get bus tickets details
 * @param {string} busID 
 * @param {string} ticketStatus 
 * @param {number} ticketNum 
 */
const getTickets = async (busID, ticketStatus, ticketNum) => {
    if (busID == undefined) {
        return;
    }

    let tickets = await schema.getTickets(busID, ticketStatus, ticketNum);

    if (tickets == undefined) {
        return;
    }

    return tickets.map(ticket => {
        return {status: ticket.status,number: ticket.number};
    });
};

/**
 * @function - update ticket status/person details
 * @param {person} - person details to be stored
 * @param {status} - ticket's status
 * @param {ticketNum} - ticket number
 * @param {busID} - bus ID whose ticke to update
 */
const updateTicket = async ({person, status, ticketNum, busID}) => {
    let personID;
    if (person != undefined) {
        personID = await personS.savePerson(person);
        if (personID == undefined) {
            return
        }
        if (personID.error != undefined) {
            return personID.error;
        }
        status = 'closed';
    }

    if (status == 'open') {
        personID = null;
    }

    return await schema.updateTicket(busID, ticketNum, status, personID);
};

/**
 * @function - fetch person details for bus and ticket number
 * @param {string} busID 
 * @param {number} ticketNum 
 */
const getPersonDetails = async (busID, ticketNum) => {
    if (busID == undefined) {
        return;
    }

    if (ticketNum == undefined) {
        return;
    }

    let person = await schema.getPersonID(busID, ticketNum);
    if (person == null) {
        return null;
    }

    if (person == undefined) {
        return;
    }

    let personData = await personS.getPerson(person);
    if (personData == undefined) {
        return;
    }
    if (personData == null) {
        return null;
    }

    return personData;
};

/**
 * @function resets all the tickets
 * @param {string} busID 
 */
const resetTickets = async (busID) => {
    if (busID == undefined) {
        return;
    }

    const res =  await schema.resetTickets(busID);
    if (res == undefined) {
        return
    }

    return res.nModified;
}

module.exports =  {
    createNewTickets,
    getTickets,
    updateTicket,
    getPersonDetails,
    resetTickets
};
/**
 * @file - bus specific application logic irrspective of transport layer
 */
const schema = require('./schema');
const ticket = require('./../ticket/ticket');

/**
 * @function - creates a new bu
 * @param {string} busName - name of the bus (required)
 */
const newBus = async ({name}) => {
    if (name == undefined || name.trim() == '') {
        return;
    }

    let busID = await schema.saveBus(name);
    if (busID == undefined) {
        return;
    }

    success = await ticket.createNewTickets(busID);
    if (success) {
        return busID;
    }
};

/**
 * @function - checks whether a bus exists
 * @param {string} busID - ID for the bus (required)
 */
const checkBus = async (busID) => {
    if (busID == undefined) {
        return false;
    }

    const exists = await schema.busExists(busID);
    if (exists == undefined) {
        return;
    }
    return exists;
}

module.exports = {
    newBus,
    checkBus,
};